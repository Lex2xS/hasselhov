# -*- coding: utf-8 -*-

"""This module is a bingo game.
Takes a file with words or sentences to use as input.
Generate random gris containing words.
Supports several player.
Any player can add a words that has been crossed.
The game ends when a player finish a line or column or when !bingo stop.

The game is used as follow: each command must start with !bingo and continue
with one of the option below:
- start : start a bingo session
- stop : stop a bingo session
- join : a player uses it to join current session
- add [word] : add a word in the list of said words, case-insensitive
- grid : see your own grid
- score : see scores
"""

###############################################################################
# IMPORTS                                                                     #
###############################################################################

from __future__ import print_function
from random import randint
try:
    import hasselang as lang
    from hasselib import herror, htools
except ImportError as ie:
    print("Error: " + str(ie) + " in " + str(__file__))
    exit()

###############################################################################
# CONSTANTS AND MESSAGES                                                      #
###############################################################################

BINGO_HELP = (
    "Bingo time! Voici les commandes:",
    "- start : Démarrer une session de bingo",
    "- stop : Terminer une session de bingo",
    "- join : Participer au bingo",
    "- grid : Voir sa grille",
    "- add [mot entendu] : Ajouter un mot pour remplir la grille pour tous les joueurs",
    "- score : Voir son propre score",
)

BINGO_SIZE = 4

START_CMD = "start"
STOP_CMD = "stop"
JOIN_CMD = "join"
GRID_CMD = "grid"
SCORE_CMD = "score"
ADD_CMD = "add"

START_MSG = "DEBUT DE LA SESSION DE BINGO !"
STOP_MSG = "FIN DE LA SESSION !"
JOIN_MSG = "JOUEUR AJOUTE !"
ADD_MSG = "MOT AJOUTE ! (!bingo grid pour voir vos grilles)"
WINNERS_FMT_MSG = "BINGOOOOOOOOOOOOOOOOOO !!!! Winner(s): {0}"
PLAYERS_FMT_MSG = "Joueurs: {0}"
SCORES_FMT_MSG = "Scores: {0}"

BINGO_RTFM = "rtfm"

BINGO_ERROR = "Désolé, le bingo est cassé !"
BINGO_START_ERROR = "Il y a déjà une session en cours ! (!bingo stop)"
BINGO_STOP_ERROR = "Aucune session en cours ! (!bingo start)"
BINGO_JOIN_ERROR = "Ce joueur participe déjà !"
BINGO_GRID_ERROR = "Aucune grille pour ce joueur !"
BINGO_ADD_ERROR = "Il n'y a rien à ajouter !"

###############################################################################
# BINGO CLASS                                                                 #
###############################################################################

class Bingo(object):
    """Bingo class, process all commands and store current game."""

    def __init__(self):
        self.__players = None
        self.__grids = None
        self.__is_started = False
        self.__is_hs = False
        self.__bingo_list = []
        self.cmd = {
            START_CMD: self.__start,
            STOP_CMD: self.__stop,
            JOIN_CMD: self.__add_player,
            GRID_CMD: self.__get_grid,
            SCORE_CMD: self.__get_score,
            ADD_CMD: self.__add_score
        }

    def add_base_files(self, *filename):
        """Add the content of a file to the database."""
        for entry in filename:
            try:
                self.__bingo_list += htools.parse_file(entry)
            except herror.ToolsHasselError as the:
                herror.warning(the, entry)
        self.__is_hs = not len(self.__bingo_list)

    def run(self, message, sender):
        """Process a command message from bingo"""
        if self.__is_hs:
            return BINGO_ERROR
        self.__answer = BINGO_HELP
        message = message.split()
        if len(message) > 1 and message[1].lower() in self.cmd.keys():
            self.__answer = self.cmd[message[1].lower()](message[2:], sender)
        return self.__answer

    def __start(self, _, sender):
        """Initialize bingo session."""
        if self.__is_started:
            return BINGO_START_ERROR
        self.__players = []
        self.__grids = {}
        self.__is_started = True
        return (START_MSG, self.__add_player(_, sender))

    def __stop(self, _, sender):
        """Stop the game, does not reset info."""
        if not self.__is_started:
            return BINGO_STOP_ERROR
        self.__is_started = False
        return "{0} ({1})".format(STOP_MSG,
                                  SCORES_FMT_MSG.format(" / ".join(self.__get_score(_, sender))))

    def __add_player(self, _, sender):
        """Add a player to the game when she uses the command join."""
        if not self.__is_started:
            return BINGO_STOP_ERROR
        if sender in self.__players:
            return BINGO_JOIN_ERROR
        self.__players.append(sender)
        if sender not in self.__grids:
            self.__grids[sender] = self.__gen_grid()
        return "{0} ({1})".format(JOIN_MSG,
                                  PLAYERS_FMT_MSG.format(", ").join(self.__players))

    def __get_grid(self, _, sender):
        """Returns the score of all players"""
        if not self.__is_started:
            return BINGO_STOP_ERROR
        if not sender in self.__players:
            return BINGO_GRID_ERROR
        grid = []
        for i in range(BINGO_SIZE):
            line = ["{0}: ".format(sender)]
            for j in range(BINGO_SIZE):
                if self.__grids[sender][i][j][1]:
                    line.append("--{0}--".format(self.__grids[sender][i][j][0]))
                else:
                    line.append("{0}".format(self.__grids[sender][i][j][0]))
            grid.append(" | ".join(line))
        return grid

    def __get_score(self, _1, _2):
        """Return the score of all players."""
        # We do not check is_started because we need score after game has ended
        if not len(self.__players):
            return BINGO_STOP_ERROR
        score = []
        for player in self.__players:
            score.append("{0}: {1}/{2}".format(player, self.__check_score(player),
                                               str(BINGO_SIZE*BINGO_SIZE)))
        return score

    def __add_score(self, message, _):
        if not len(message):
            return BINGO_ADD_ERROR
        try:
            ok = False
            for player in self.__players:
                for i in range(BINGO_SIZE):
                    for j in range(BINGO_SIZE):
                        if self.__grids[player][i][j][0].lower() == " ".join(message).lower():
                            self.__grids[player][i][j][1] = True
                            ok = True
            if not ok:
                return BINGO_ADD_ERROR
        except KeyError as ke:
            print("ERROR: " + str(ke))
            return BINGO_ADD_ERROR
        return ADD_MSG

    # Create grid

    def __gen_grid(self):
        """Generate bingo grid with specified dimensions from bingo list."""
        # Init
        grid = [[""] * BINGO_SIZE for _ in range(BINGO_SIZE)]
        save = []
        # Fill
        i = 0
        while i < BINGO_SIZE:
            j = 0
            while j < BINGO_SIZE:
                word = self.__bingo_list[randint(0, len(self.__bingo_list) - 1)]
                if word not in save:
                    grid[i][j] = [word, False] # List [word, score]
                    save.append(word)
                    j += 1
            i += 1
        return grid

    # Check player score

    def __check_score(self, player):
        """Check the current score of a player."""
        final = 0
        for i in range(BINGO_SIZE):
            final += len([x for x in self.__grids[player][i] if x[1]])
        return str(final)

    def is_bingo(self):
        if not self.__is_started:
            return None
        winners = []
        for player in self.__players:
            # Check line
            for i in range(BINGO_SIZE):
                ct = len([x for x in self.__grids[player][i] if x[1]])
                if ct == BINGO_SIZE:
                    winners.append(player)
            # Check column
            for j in range(BINGO_SIZE):
                ct = 0
                for i in range(BINGO_SIZE):
                    ct += 1 if self.__grids[player][i][j][1] else 0
                if ct == BINGO_SIZE:
                    winners.append(player)
        if len(winners):
            self.__is_started = False
            return WINNERS_FMT_MSG.format(", ".join(winners))
        return None
