# -*- coding: utf-8 -*-
#
# Hasselhov bot - Lex2xS
#
# Tools

# pylint: disable=invalid-name

from os import path, access, R_OK
from os import path
from re import split
try:
    import hasselang as lang
except ImportError as ie:
    print("Error: " + str(ie) + " in " + str(__file__))
    exit()

"""Useful generic functions to be used everywhere in the code."""

# STRING MANAGEMENT
def remove_special(string):
    """Remplace special characters with ASCII equivalent"""
    srch = ('ç', 'ô', 'û', 'â', 'à', 'î', 'ï', 'é', 'è', 'ê', 'ë')
    repl = ('c', 'o', 'u', 'a', 'a', 'i', 'i', 'e', 'e', 'e', 'e')
    for i in range(0, len(srch)):
        string = string.replace(srch[i], repl[i])
    return string

def clean_grammar(content):
    """Change sentence syntax to adapt it to french grammar"""
    for key, value in lang.GRAMMAR.iteritems():
        content = content.replace(key, value)
    return content

# PATH AND FILE MANAGEMENT

def get_file_directory():
    """Return main source directory of StaGR.
    We get the parent directory of this file's directory because it is in a
    module package directory, which is StaGR/sgrlib when wee want StaGR.
    """
    return path.dirname(path.realpath(path.join(__file__, path.pardir)))

def is_file_access(filename):
    """Check that file exists and has the right permissions."""
    if not filename:
        return False
    filepath = realpath(filename)
    if not path.exists(filepath):
        return False
    elif not path.isfile(filepath) or not access(filepath, R_OK):
        return False
    return True

def realpath(filename, force=False):
    """When using only a filename with no path, we want the real path to
    point to source directory and not to current working directory.
    Therefore, if the filename is a path, we get the normal absolute path.
    If it's only a filename, we get the realpath with script path.
    """
    if not filename:
        return None
    if path.sep not in filename or force:
        filename = path.join(get_file_directory(), filename)
    return path.realpath(filename)

def parse_file(filename):
    """Try to parse a file in DATABASE_DIR after checking it.
    Returns a list of parsed words or sentences.
    Raises ToolsHasselHerror.
    """
    content = []
    base = path.join(get_file_directory(), lang.DATABASE_DIR, filename)
    if is_file_access(base):
        try:
            with open(base, 'r') as fd:
                content = [x for x in split(lang.LINESPLIT, fd.read()) if len(x) > 1]
        except IOError as ioe:
            raise ToolsHasselError(herror.INVALIDFILE, filename)
    else:
        raise ToolsHasselError(herror.INVALIDFILE, filename)
    return content
