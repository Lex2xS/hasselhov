# -*- coding: utf-8 -*-
#
# Markov for Hasselhov - Lex2xS
#
# Main class

# pylint: disable=invalid-name


"""Class for Markov chain sentences generator.
Principle : A sentence is generated either starting with a random word,
or from a word detected in a trigger sentence.
Two words are used to predict the next one or previous one according to
what these words are usually met with in the sentences contained in the
database.
"""

###############################################################################
# IMPORTS                                                                     #
###############################################################################

from __future__ import print_function
from os import listdir
from os.path import realpath, isdir, join
from random import randint, triangular, choice
from re import split

try:
    from hasselib import herror, htools
except ImportError as ie:
    print("Error: " + str(ie) + " in " + str(__file__))
    exit()


###############################################################################
# CONSTANTS                                                                   #
###############################################################################

LINESPLIT = "[\.!\n]+"
WORDSPLIT = "[\ ]+"

CHARWHITELIST = (" ", ",", ";", "\'", "-")
CHARBLACKLIST = ("\"", "(", ")")
PUNCTWHITELIST = (",", "?", "!")

MIN_MATCH = 3 # Size of words worth noticing when parsing.

STARTING = "#@#"
TERMINAL = "@#@"

###############################################################################
# MARKOV CLASS                                                                #
###############################################################################

class Token(object):
    """A token represent a word with different properties, including:
    - A list of words found before
    - A list of words found after
    - Properties such as: it can start or end a sentence."""
    
    def __init__(self, key):
        self.key = key
        self.__before = []
        self.__after = []

    def add(self, before, after):
        """Add new words found before and after the specific key."""
        if not before or not after:
            raise herror.MarkovHasselError(herror.ADDMISUSE)
        self.__before.append(before)
        self.__after.append(after)

    def get_number(self):
        """Returns the number of times the word has been encountered."""
        return len(self.__before)

    def get_random_match(self):
        """Randomly select an existing before or after word and returns it
        as a list [key, after] or [before, key].
        """
        position = choice((STARTING, TERMINAL))
        if position == STARTING:
            return [choice(self.__before), self.key]
        if position == TERMINAL:
            return [self.key, choice(self.__after)]

    def get_before(self, after, must_finish=False):
        """Get a before word for key according to the word following it.
        If no match is found, return a before word without matching with
        the after.
        When must_finished is set, return a STARTING before if found in list.
        """
        if must_finish and STARTING in self.__before:
            # Probability: number of occurrences of STARTING
            if randint(0, len(self.__before)) in range(self.__before.count(STARTING)):
                return STARTING
        befores = [self.__before[i] for i in range(len(self.__after)) \
                   if self.__after[i] == after and self.__before[i] != STARTING]
        if len(befores):
            return choice(befores)
        return choice(self.__before)


    def get_after(self, before, must_finish=False):
        """Get a after word for key according to the word preceding it.
        If no match is found, return a after word without matching with
        the before.
        When must_finished is set, return a TERMINAL after if found in list.
        """
        if must_finish and TERMINAL in self.__after:
            # Probability: number of occurrences of TERMINAL
            if randint(0, len(self.__after)) in range(self.__after.count(TERMINAL)):
                return TERMINAL
        afters = [self.__after[i] for i in range(len(self.__before)) \
                  if self.__before[i] == before and self.__after[i] != TERMINAL]
        if len(afters):
            return choice(afters)
        return choice(self.__after)

    def __str__(self):
        return "{0}:\nB: {1}\nA: {2}".format(self.key, ", ".join(self.__before),
                                             ", ".join(self.__after))

class Markov(object):
    """This class contains methods to:
    - Parse the content of a database and store it so that it is used to
      generate the sentences
    - Detect the most common word in a sentence in order to reply to it
    - Generate a sentence based on this word (or a random one)
    """
    def __init__(self):
        self.__language = {} # Base
        self.__size_min = 0 # Min encountered sentence size
        self.__size_max = 0 # Max encountered sentence size
        self.__history = [] # Keep in mind the file that were already parsed
        self.__size_history = [] # Keep history of sizes

    def add_base_files(self, *filename):
        """Add the content of a file to the database."""
        for entry in filename:
            entry = realpath(entry)
            if entry in self.__history:
                continue
            try:
                sentences = htools.parse_file(entry)
                sentences = [htools.remove_special(x).lower() for x in sentences]
                self.__history.append(entry)
                for sentence in sentences:
                    self.__learn(sentence)
            except herror.ToolsHasselError as the:
                herror.warning(herror.INVALIDFILE, entry)
            except herror.MarkovHasselError as mhe:
                herror.warning(str(mhe))
        # DEBUG
        # for word in self.__language:
        #     print(self.__language[word])


    def add_base_dir(self, *directories):
        """Add the content of a directory (all files) to the database."""
        for directory in directories:
            if isdir(directory):
                self.add_base_files(*[realpath(join(directory, x)) for x in listdir(directory)])


    def answer(self, entry):
        """Generated a sentence based on an input sentence, so that the generator
        interacts with external entity.
        The interaction is performed based on the least common word detected in the
        sentence which also exist in the dictionary. If no common word is found, we
        generate a random sentence."""
        seed = self.__least_common_word(htools.remove_special(entry).lower())
        seed = seed if seed else self.__get_random_seed()
        try:
            answer = self.__build(seed)
        except herror.MarkovHasselError as mhe:
            herror.warning(str(mhe))
        return answer


    def __set_sizes(self, size):
        """Change minimum and maximum sizes according to the values received"""
        # Initialize
        if not self.__size_min and not self.__size_max:
            self.__size_min = size
            self.__size_max = size
        # Change values if needed
        self.__size_min = size if size < self.__size_min else self.__size_min
        self.__size_max = size if size > self.__size_max else self.__size_max
        # Reset average
        self.__size_history.append(size)
        self.average_size = int(sum(self.__size_history) / float(len(self.__size_history)))


    def __clean(self, sentence):
        """Remove blacklisted chars from sentence."""
        for char in CHARBLACKLIST:
            sentence = sentence.replace(char, '')
        return sentence.lower()

    def __handle_punct(self, words):
        """Isolate punctuation such as comma, they are considered as words."""
        ct = 0
        for word in words:
            if len(word) > 1 and word[-1] in PUNCTWHITELIST:
                char = word[-1]
                words[ct] = word.replace(char, '')
                words.insert(ct + 1, char)
            ct += 1
        return words

    def __learn(self, sentence):
        """Store sentences in __language."""
        sentence = self.__clean(sentence)
        words = [x for x in split(WORDSPLIT, sentence) if len(x) > 0]
        words = self.__handle_punct(words)
        self.__set_sizes(len(words))
        # Put it in the language base
        for ct in range(len(words)):
            before = STARTING if ct - 1 < 0 else words[ct - 1]
            after = TERMINAL if ct + 1 == len(words) else words[ct + 1]
            try:
                if words[ct]  not in self.__language:
                    self.__language[words[ct]] = Token(words[ct])
                self.__language[words[ct]].add(before, after)
            except herror.MarkovHasselError as mhe:
                herror.error(str(mhe))

    def __get_random_seed(self):
        """Get a random word from the language to start from."""
        clean_basis = [x for x in self.__language if len(x) > MIN_MATCH]
        return clean_basis[randint(0, len(clean_basis) - 1)]

    def __least_common_word(self, entry):
        """Detect the least common word from the entry, which also exists in the
        learning basis.
        """
        least_common = None
        minimum = 0
        for word in split(WORDSPLIT, entry):
            if len(word) > MIN_MATCH and word in self.__language:
                count = self.__language[word].get_number()
                least_common = word if count < minimum or minimum == 0 else least_common
                minimum = count if count < minimum or minimum == 0 else minimum
        return least_common

    def __get_random_size(self):
        """Get the size of the sentence to generate according to minimum size,
        maximum size and most of all: total average size.
        """
        return int(triangular(self.__size_min, self.average_size, self.__size_max + 1))

    def __build(self, seed):
        """Build a sentence according to what is found in language and based on seed."""
        # Define sentence size, the aim is to make sure that the final sentence
        # stays close to the limits. When approaching it, we urge the function to
        # stop as soon as possible.
        size = self.__get_random_size()
        threshold = (size / 2) + (size / 4) # 3/4
        must_finish = False
        # As we match words two by two, we first need to match seed with another word
        # This word will be randomly chosen from before or after list. Returns a list:
        # either [before, seed] or [seed, after]
        sentence = self.__language[seed].get_random_match()
        while True:
            # Add a word before
            if sentence[0] != STARTING:
                postbefore = sentence[1] if sentence[1] != TERMINAL else ""
                before = self.__language[sentence[0]].get_before(postbefore, must_finish)
                sentence.insert(0, before)
            # Add a word after (only if not already finished)
            if sentence[-1] != TERMINAL:
                preafter = sentence[-2] if sentence[-2] != STARTING else ""
                after = self.__language[sentence[-1]].get_after(preafter, must_finish)
                sentence.append(after)
            # Threshold and conclusion to the sentence
            if len(sentence) >= threshold:
                must_finish = True
            if sentence[0] == STARTING and sentence[-1] == TERMINAL:
                break
        # Clean sentence and return
        sentence = [x for x in sentence if x not in (STARTING, TERMINAL)]
        return " ".join(sentence).replace(" ,", ",").capitalize()
