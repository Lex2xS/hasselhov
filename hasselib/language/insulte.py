# -*- coding: utf-8 -*-
#
# Insulte for Hasselhov - Lex2xS
#
# Main class

# pylint: disable=invalid-name

"""Classes for insult management. There are different types of classes:
- InsulteManager: unique class for insult generator management, everything
  goes through it.
- InsulteGenerator: Generator for insulte (one type so far, can add subclasses
  later. There is one InsulteGenerator instanciated by type of input. All
  generators are centralized in a list in InsulteManager.
"""

###############################################################################
# IMPORTS                                                                     #
###############################################################################

from __future__ import print_function
from os import path
from random import choice, randint
import re
#XML Stuff
from xml.parsers.expat import ExpatError
from xml.etree import ElementTree as etxml

try:
    import hasselang as lang
    from hasselib import herror, htools
except ImportError as ie:
    print("Error: " + str(ie) + " in " + str(__file__))
    exit()


###############################################################################
# INSULTE MANAGER                                                             #
###############################################################################

class InsulteManager(object):
    """InsulteManager: Handles generators and defines the appropriate behavior
    according to the original insulte command (choose sender, receiver and
    type of InsulteGenerator to be used.
    """
    __generators = None

    def __init__(self):
        self.__generators = []

    def add(self, insulte_file):
        """Takes an insult file (XML) and uses it to create an InsulteGenerator
        object. File format is described in InsulteGenerator.
        """
        try:
            generator = InsulteGenerator(insulte_file)
            self.__generators.append(generator)
        except herror.InsulteHasselError as ihe:
            herror.warning(str(ihe))

    def get_receiver(self, message, sender):
        """Finds out who is going to be insulted.
        If receiver = BOT_NICKNAME, we return the name of the sender instead.
        """
        m = re.match("[^ ]* insulte ([^ ]*)", message, re.IGNORECASE)
        if not m:
            return sender
        return m.group(1) if m.group(1) != lang.BOT_NICKNAME else sender

    def answer(self, receiver, sender, generator=None):
        """Build an insulte using the specified generator, or a random
        one from __generators if generator is set to None.
        """
        gen = generator if generator else choice(self.__generators).run()
        if not gen:
            raise herror.InsulteHasselError(herror.NOINSULTE)
        # No one should insult the bot master, except the bot master herself
        if receiver == lang.BOT_MASTER and sender != lang.BOT_MASTER:
            receiver = sender
        return("{1} {0}".format(receiver, gen))

###############################################################################
# INSULTE GENERATOR                                                           #
###############################################################################

class InsulteGenerator(object):
    """Insulte generator instanciated by InsulteManager according to an
    insulte file specified (searched for in INSULTE_DIR.
    An insulte has the following format:
    *** presulte sulte postsulte ***
    - presulte and postsulte are optional
    - insult can be prefixed with "espèce de"
    An insulte file is a specific XML file with the following format:
    <presulte>list, of, presulte, items</presulte>
    <sulte>list, of, sulte, items</sulte>
    <postsulte>list, of, postsulte, items</postsulte>
    """
    categories = (lang.PRESULTE, lang.SULTE, lang.POSTSULTE)
    __insultes = {}
    __name = None

    def __init__(self, insulte_file):
        """Creates the generator with content of insulte_file."""
        self.__name = path.basename(insulte_file)
        self.__insultes = {}
        try:
            with open(insulte_file, 'r') as fd:
                xml = etxml.parse(fd)
        except (IOError, ExpatError):
            raise herror.InsulteHasselError(herror.INVALIDFILE)
        self.__load_xml(xml)

    def __load_xml(self, xml):
        """Extract insult data from XML tree in argument.
        The XML file has the following format:
        <presulte>list, of, presulte, items</presulte>
        <sulte>list, of, sulte, items</sulte>
        <postsulte>list, of, postsulte, items</postsulte>
        """
        xmliter = xml.getiterator(lang.INSULTE_TAG)
        for gentype in xmliter:
            for category in gentype.getchildren():
                if category.tag in self.categories:
                    content = [x.strip().encode('utf-8') for x in category.text.replace("\n", " ").split(",")]
                    self.__insultes[category.tag] = content

    def run(self):
        """Generate insulte with format:
        [espèce de] [presulte] sulte [postsulte]
        """
        insulte = []
        # Espèce de
        if randint(0, 2) == 1:
            insulte.append(lang.INSULTE_PRE.encode('utf-8'))
        # Presulte
        if randint(0, 3) != 1:
            insulte.append(choice(self.__insultes[lang.PRESULTE]))
        # Sulte
        insulte.append(choice(self.__insultes[lang.SULTE]))
        # Postsulte
        if randint(0, 3) != 1:
            insulte.append(choice(self.__insultes[lang.POSTSULTE]))
        return htools.clean_grammar(" ".join(insulte))
