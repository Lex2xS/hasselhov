# -*- coding: utf-8 -*-
#
# Hasselhov bot - Lex2xS
#
# Error management (constants and functions)

# pylint: disable=invalid-name


"""Error management module for Hasselhov. This module contains:
* Constants for error code and error messages
* Error handling and display functions
"""

###############################################################################
# IMPORTS                                                                     #
###############################################################################

from __future__ import print_function

###############################################################################
# CONSTANTS                                                                   #
###############################################################################

INVALIDFILE = "File is invalid"
EMPTYFILE = "File is empty"
INVALIDLINE = "Config line is invalid"
ADDMISUSE = "Words must have a before and an after word"

NOINSULTE = "No insult generator found"

###############################################################################
# FUNCTIONS                                                                   #
###############################################################################

def warning(message, value=None):
    """Displays a warning message on standard output."""
    if value: # Add the value to the message
        message = "{0} ({1})".format(message, value)
    print("WARNING: {0}.".format(message))

def error(message, value=None):
    """Displays an error message on standard output and exits."""
    if value: # Add the value to the message
        message = "{0} ({1})".format(message, value)
    print("ERROR: {0}.".format(message))
    exit()

def exception(message, display=True, quit=False):
    """Displays the exception message on standard output and exits
    if quit is set to True."""
    if display:
        print(message)
    if quit:
        exit()

###############################################################################
# CUSTOM EXCEPTIONS                                                           #
###############################################################################

# Base classes

class HasselError(Exception):
    """Base class for Hasselhov exceptions classes."""

    def __init__(self, message="", argument=""):
        self.exctype = self.__class__.__name__
        self.message = message
        self.argument = argument
        super(HasselError, self).__init__()

    def __str__(self):
        if self.argument:
            return "HasselException: {0}: {1} ({2})".format(self.exctype,
                                                             self.message,
                                                             self.argument)
        return "HasselException: {0}: {1}".format(self.exctype,
                                                    self.message)

class ToolsHasselError(HasselError):
    """Module loading exceptions."""
    pass

class ModuleHasselError(HasselError):
    """Module loading exceptions."""
    pass

class TriggerHasselError(HasselError):
    """Module loading exceptions."""
    pass

class ActionHasselError(HasselError):
    """Module loading exceptions."""
    pass


class MarkovHasselError(HasselError):
    """Markov learning and building exceptions."""
    pass

class InsulteHasselError(HasselError):
    """Insulte parsing and building exceptions."""
    pass
