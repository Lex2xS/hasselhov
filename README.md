Hasselhov
=========
Building bad intelligence since 2018

Introduction
------------

This repository contains a set of tools and modules for sentences generation
and bots that use them.

Principle
---------

The bot takes several modules that can be added and removed from the bot.
The aim of modules is to be able to add and remove behaviors easily.

Modules to implement:

[ ] Request from API : The bot requests to a specified API and send a message
    with the result (ex: kmlproject.com).
[ ] Commander : The bot has a master which can talk on her behalf (back to
    manual control). The name of the master is defined in config.
[ ] Markov : The bot speaks with Markov chains and is trigger when reading
    its name or predefined keywords.
[ ] Simple trigger : The bot has predefined answer to say when triggered
    on specific keywords or sentences.

Config
------

The bot is configured via command line and via a configuration file.
There is a single configuration file for global config, and module
configuration (separated by section)