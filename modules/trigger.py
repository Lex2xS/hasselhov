# -*- coding: utf-8 -*-

"""Triggers are conditions to check, that will trigger an action.
Trigger -> action -> output of action (message to be displayed

This module contains the abstract class of trigger classes.
All specialized trigger class shall inherit from ATrigger, and therefore
follow their base class implementation principe.

Implementation principles:
- A trigger has a type which is used to identify the trigger
  -> A type is a string defined by the creator of the trigger
     It will be used in the configuration file to refer to the trigger 
- The method is_triggered is called to check if the trigger occurs.
  -> The is_triggered method returns True if all trigger conditions are met,
      else False.

The factory can be used to return the appropriate ATrigger's subclass instance
according to the type given as parameter.
"""

###############################################################################
# IMPORTS                                                                     #
###############################################################################

from __future__ import print_function
from abc import ABCMeta, abstractmethod # Abstract class
from random import randint
try:
    import hasselang as lang
    from hasselib import herror, htools
except ImportError as ie:
    print("Error: " + str(ie) + " in " + str(__file__))
    exit()

###############################################################################
# ACTION ABSTRACT OBJECTS, should not be modified                            #
###############################################################################

def factory(ftype):
    """Instantiate the appropriate ATrigger-derived class depending on
    the specified type of trigger in config.
    Returns the corresponding subclass.
    """
    for cls in ATrigger.__subclasses__():
        if cls._type == ftype:
            return cls()
    raise herror.ModuleHasselError(lang.W_UNKNOWNTYPE_T, ftype)

class ATrigger(object):
    """Trigger abstract class. A trigger is used by a module to know when to
    run an action (if trigger exists: run action).
    A trigger has: - A type (according to ATrigger subclasses)
                   - Parameters according to its type (subclass-dependent)
                   - A method checking if trigger is... triggered (return bool)
    A trigger is set according to the config file.
    """
    __metaclass__ = ABCMeta
    # Protected attributes
    _type = None # String
    _proba = 100

    def TRUE(self): return randint(1, 100) in range(1, self._proba)
    def FALSE(self): return False

    @abstractmethod
    def is_triggered(self):
        """Check if the condition that this objects looks for is met. Returns
        a boolean: True if condition is met, else False.
        Default is True.
        """
        return True

    def _set_proba(self, value):
        """If value is an integer between 0 and 100, use it as proba."""
        if value.isdigit():
            # print(value)
            proba = int(value)
            if proba in range(1, 100):
                self._proba = proba
                return True
        return False


    def _replace(self, answer, message=None, sender=None):
        # Does not require a specific message
        answer = answer.replace(lang.ME_VAR, lang.BOT_NICKNAME)
        answer = answer.replace(lang.MASTER_VAR, lang.BOT_MASTER)
        if sender:
            answer = answer.replace(lang.SENDER_VAR, sender)
        return answer


    def __str__(self):
        return "TRIGGER {0}".format(self._type)

###############################################################################
# TRIGGER SUBCLASSES                                                          #
###############################################################################

#-----------------------------------------------------------------------------#
# JOIn TRIGGER                                                                #
# In config: ON JOIN user                                                     #
# Triggers when join event occurs.                                            #
#-----------------------------------------------------------------------------#

class JoinTrigger(ATrigger):
    """Trigger JOIN event. Inherits from ATrigger.
    Triggers according to the user triggering the join event:
    - ME: Triggers when the bot connects.
    - USERS: Triggers when any user connects.
    - Specific username(s) list: Triggers when they connect.
    """
    _type = lang.JOIN_EVENT
    __all = False
    __list = None

    def init(self, _argument_table):
        """Check and parse argument_table."""
        argument_table = _argument_table[_argument_table.index(self._type)+1:]
        if not len(argument_table):
            raise TriggerHasselError(herror.INVALIDLINE, " ".join(_argument_table))
        self.__list = []
        if argument_table[0] == lang.ME_VAR:
            self.__list.append(lang.BOT_NICKNAME)
        elif argument_table[0] == lang.ALL_VAR:
            self.__all = True
        else:
            for value in argument_table:
                self.__list.append(value)

    def is_triggered(self, message, sender):
        """Check if the condition that this objects looks for is met. Returns
        a boolean: self.TRUE if condition is met, else self.FALSE.
        Default is True.
        """
        interlist = self.__list[:]
        context_list = [self._replace(x, message, sender) for x in interlist]
        if self.__all:
            return self.TRUE()
        elif sender in context_list:
            return self.TRUE()
        return self.FALSE()

#-----------------------------------------------------------------------------#
# CONTAINS TRIGGER                                                            #
# In config: ON MESSAGE CONTAINS value1, value2                               #
# Triggers when a message contains a value from the list                      #
#-----------------------------------------------------------------------------#

class ContainsTrigger(ATrigger):
    """Check if the original message CONTAINS one or more words specified
    in configuration files. There can be multiple words detected simultaneously.
    For example: ON MESSAGE CONTAINS lama, du poulet, 10 moutons
    (Words are case insentitive).
    """
    _type = lang.CONTAINS_TRIGGER
    __list = None

    def init(self, argument_table):
        """Set up a list of words to look for."""
        # Check if last argument is proba
        if len(argument_table) > 1:
            ret = self._set_proba(argument_table[-1])
            if ret:
                argument_table = argument_table[:len(argument_table)-1]
        argument_string = " ".join(argument_table[argument_table.index(self._type)+1:])
        # Last argument as probability
        self.__list = []
        for item in argument_string.split(","):
            item = item.split()
            if len(item) == 2 and item[0] == lang.FILE_KEYWORD:
                try:
                    self.__list += htools.parse_file(item[1])
                except herror.ToolsHasselError as the:
                    herror.warning(the)
            else:
                # To lower except keywords
                for word in item:
                    if not item[0].startswith(lang.VAR_TAG) and \
                       not item[0].endswith(lang.VAR_TAG):
                        item[0] = item[0].lower()
                # Just one sentence (we accept several words)
                item = " ".join(item)
                self.__list.append(item.strip())

    def is_triggered(self, message, sender):
        """Check if the message contains a value from __list"""
        # First we check if word is found in string, but it may be contained
        # within another work (and we can't check word by word because there
        # can be more than one word to look for.
        message = message.lower()
        context_list = [self._replace(x, message, sender).lower() for x in self.__list]
        for entry in context_list:
            try:
                begin = message.index(entry)
                end = begin + len(entry)-1
                # We check that these are isolated words:
                if (begin == 0 or not message[begin-1].isalnum()) \
                   and (end == len(message)-1 or not message[end+1].isalnum()):
                    return self.TRUE()
            except ValueError as ve:
                pass # We do nothing, it does not matter :)
        return self.FALSE()

#-----------------------------------------------------------------------------#
# COMMAND TRIGGER                                                             #
# In config: ON MESSAGE COMMAND command                                       #
# Triggers when the message start with a specific command                     #
#-----------------------------------------------------------------------------#

class CommandTrigger(ATrigger):
    """Check if the message starts with a command as defined in config.
    Config usually contain arguments that will be given to the corresponding
    action Case sensitive.
    """
    _type = lang.COMMAND_TRIGGER
    __command = None

    def init(self, _argument_table):
        """Extract the command to look for."""
        argument_table = _argument_table[_argument_table.index(self._type)+1:]
        if not len(argument_table):
            raise TriggerHasselError(herror.INVALIDLINE, " ".join(_argument_table))
        # Last argument as probability
        if len(argument_table) > 1:
            self._set_proba(argument_table[-1])
        self.__command = argument_table[0]

    def is_triggered(self, message, sender):
        if message.startswith(self.__command):
            if len(message) == len(self.__command) or \
               not message[len(self.__command)].isalnum():
                return self.TRUE()
        return self.FALSE()

#-----------------------------------------------------------------------------#
# SENDER TRIGGER                                                              #
# In config: ON SENDER sender                                                 #
# Triggers when the message sender is a specific user                         #
#-----------------------------------------------------------------------------#

class SenderTrigger(ATrigger):
    """Triggers when a user sends a message. A probability can be set so that
    the triggers does not work all the time.
    """
    _type = lang.SENDER_TRIGGER
    __sender = None

    def init(self, _argument_table):
        """Extract the sender and the proba."""
        argument_table = _argument_table[_argument_table.index(self._type)+1:]
        if not len(argument_table):
            raise TriggerHasselError(herror.INVALIDLINE, " ".join(_argument_table))
        # Last argument as probability
        if len(argument_table) > 1:
            self._set_proba(argument_table[-1])
        self.__sender = self._replace(argument_table[0])

    def is_triggered(self, message, sender):
        """May return False if sender is the right one but random is outside
        probabilities set.
        """
        if sender == self.__sender:
                return self.TRUE()
        return self.FALSE()
