# -*- coding: utf-8 -*-

"""A module can be plugged to the plug to adopt multiple behaviors depending
on the context.

The context is defined as a "trigger": a condition to met that will trigger
a behavior. Example: Bot is quoted, a specific word is found in a message

A behavior is defined as an "action": a message to be returned and said by
the bot. Example: Answer generated depending on context, predefined message

Module class should not be modified. All specialization are done in trigger
and action subclasses (inheriting respectively from ATrigger and AAction.
"""

###############################################################################
# IMPORTS                                                                     #
###############################################################################

from __future__ import print_function
from modules import action
from modules import trigger
try:
    import hasselang as lang
    from hasselib import herror, htools
except ImportError as ie:
    print("Error: " + str(ie) + " in " + str(__file__))
    exit()

###############################################################################
# MODULE INTERFACE OBJECTS, should not be modified                            #
###############################################################################

def do_reload():
    reload(trigger)
    reload(action)

class Module(object):
    """Module abstract class. All modules inherit from it.
    A module has: - An event: the type of event encountered (JOIN, MESSAGE)
                  - A trigger <ATrigger> (a condition is met)
                  - an action <AAction> (the associated action is run)
                  - a priority <int> (order in which modules are run when a
                    trigger match with several modules).
    A module is set according to a config file with the following syntax:
        [Module]
        ON [event] [trigger]
        DO [action]
    Example :
        [Module]
        ON JOIN
        DO RANDOM_SENTENCE FROM file toto.txt
    """
    # Protected attributes
    event = None # Type of event. Currently supported: JOIN and MESSAGE
    trigger = None # ATrigger object
    action = None # AAction object
    priority = None # Integer

    def __init__(self, trigger_, action_, priority_):
        """Initialize the module with trigger, action and priority info
        from reading configuration."""
        # Initialize Event and Trigger (same line)
        if not len(trigger_):
            raise herror.ModuleHasselError(lang.W_SYNTAX, trigger)
        self.event = trigger_[0]
        # Join is an event AND a trigger, other have distinct event & trigger
        ttype = trigger_[0] if trigger_[0] == lang.JOIN_EVENT else trigger_[1]
        self.trigger = trigger.factory(ttype)
        try:
            self.trigger.init(trigger_)
        except herror.TriggerHasselError as the:
            raise herror.ModuleHasselError(the)
        # Initialize action
        if not len(action_):
            raise herror.ModuleHasselError(lang.W_SYNTAX, action_)
        self.action = action.factory(action_[0])
        try:
            self.action.init(action_)
        except herror.ActionHasselError as ahe:
            raise herror.ModuleHasselError(ahe)
        # Priority
        self.priority = priority_

    def __str__(self):
        """Prints info on module."""
        return "MODULE {0}\nTrigger: {1}\nAction: {2}".format(self.priority,
                                                              str(self.trigger),
                                                              str(self.action))
