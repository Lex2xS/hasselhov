# -*- coding: utf-8 -*-

"""Actions are command run when a trigger is encountered:
Trigger -> action -> output of action (message to be displayed

This module contains the abstract class of action classes.
All specialized action class shall inherit from AAction, and therefore
follow their base class implementation principe.

Implementation principles:
- An action has a type which is used to identify the action
  -> A type is a string defined by the creator of the action 
     It will be used in the configuration file to refer to the action
- The method run is called when a trigger occurs. it must contain all the
  launching process of a specific action
  -> The run method is called everytime. If any initialization is required,
     it must not be done in the run but in a initialization method.

The factory can be used to return the appropriate AAction's subclass instance
according to the type given as parameter.
"""

###############################################################################
# IMPORTS                                                                     #
###############################################################################

from __future__ import print_function
from abc import ABCMeta, abstractmethod # Abstract class
from os import path
from random import choice
from re import split
import urllib2
try:
    import hasselang as lang
    from hasselib import herror, htools
    from hasselib.language import markov, insulte
    from hasselib.game import bingo
except ImportError as ie:
    print("Error: " + str(ie) + " in " + str(__file__))
    exit()

###############################################################################
# ACTION ABSTRACT OBJECTS, should not be modified                            #
###############################################################################

def factory(ftype):
    """Instantiate the appropriate AAction-derived class depending on
    the specified type of trigger in config.
    Returns the corresponding subclass.
    """
    for cls in AAction.__subclasses__():
        if cls._type == ftype:
            return cls()
    raise herror.ModuleHasselError(lang.W_UNKNOWNTYPE_A, ftype)

class AAction(object):
    """Action abstract class. An action is run when a trigger is met.
    An action has: - A type (according to AAction subclasses)
                   - Parameters according to its type (subclass-dependent)
                   - A method running the action and returning a message (str)
    An action is set according to the config file.
    """
    __metaclass__ = ABCMeta
    # Protected attributes
    _type = None # String

    @abstractmethod
    def init(self, argument_table):
        """Initialize action arguments."""
        pass

    @abstractmethod
    def run(self, message, sender):
        """Runs the action! May need to use original trigger to do so."""
        pass

    def _replace(self, answer, message, sender):
        answer = answer.replace(lang.ME_VAR, lang.BOT_NICKNAME)
        answer = answer.replace(lang.SENDER_VAR, sender)
        answer = answer.replace(lang.MASTER_VAR, lang.BOT_MASTER)
        return answer

    def __str__(self):
        return "ACTION {0}".format(self._type)

###############################################################################
# ACTION SUBCLASSES                                                           #
###############################################################################

#-----------------------------------------------------------------------------#
# PRINT ACTION                                                                #
# In config: DO PRINT message                                                 #
# Display message as set in config when the corresponding trigger occurs.     #
#-----------------------------------------------------------------------------#

class PrintAction(AAction):
    """Runs PRINT action. Inherits from AAction.
    The message printed in defined in the configuration file. It can contain
    variables which will be replaced at run.
    """
    _type = lang.PRINT_ACTION
    __answer = None

    def init(self, argument_table):
        argument_table = argument_table[argument_table.index(self._type)+1:]
        self.__answer = " ".join(argument_table)

    def run(self, message, sender):
        """Check if the condition that this objects looks for is met. Returns
        a boolean: True if condition is met, else False.
        Default is True.
        """
        answer = self._replace(self.__answer, message, sender)
        return answer

#-----------------------------------------------------------------------------#
# RANDOM ACTION                                                               #
# In config: DO PRINT base_file                                               #
# Display a random message from base_file                                     #
#-----------------------------------------------------------------------------#

class RandomAction(AAction):
    """Runs RANDOM action. Inherits from AAction.
    Prints a random message extracted from a file defined in config file.
    The base file usually contains one sentence per line.
    """
    _type = lang.RANDOM_ACTION
    __list = None
    __answer = None

    def init(self, argument_table):
        self.__list = []
        argument_table = argument_table[argument_table.index(self._type)+1:]
        for entry in argument_table:
            try:
                self.__list += htools.parse_file(entry)
            except herror.ToolsHasselError as the:
                herror.warning(the, entry)

    def run(self, message, sender):
        """Returns a random entry from list"""
        self.__answer = choice(self.__list)
        return self.__answer

#-----------------------------------------------------------------------------#
# MARKOV ACTION                                                               #
# In config: DO MARKOV base_file                                              #
# Display message generated with Markov chain using base_file                 #
#-----------------------------------------------------------------------------#

class MarkovAction(AAction):
    """Runs MARKOV action. Inherits from AAction.
    The message printed in defined in the configuration file. It can contain
    variables which will be replaced at run.
    """
    _type = lang.MARKOV_ACTION
    __markov = None
    __answer = None

    def init(self, argument_table):
        self.__markov = markov.Markov()
        argument_table = argument_table[argument_table.index(self._type)+1:]
        for entry in argument_table:
            base = path.join(htools.get_file_directory(), lang.DATABASE_DIR, entry)
            if htools.is_file_access(base):
                self.__markov.add_base_files(base)

    def run(self, message, sender):
        """Check if the condition that this objects looks for is met. Returns
        a boolean: True if condition is met, else False.
        Default is True.
        """
        self.__answer = self.__markov.answer(message)
        return self.__answer

#-----------------------------------------------------------------------------#
# KMLAPI ACTION                                                               #
# In config: DO KMLAPI                                                        #
# Call different URLs of KML API depending on argument                        #
#-----------------------------------------------------------------------------#

LOX = "kamoulox"
SCUSE = "excuse"
SULTE = "insulte"
GRAT = "félicitations"
TACLIC = "putaclic"
KAMOU = {
    LOX: "https://www.kmlproject.com/Launcher/LKamoulox.php",
    SCUSE: "https://www.kmlproject.com/Launcher/LKamouscuse.php",
    SULTE:  "https://www.kmlproject.com/Launcher/LKamousulte.php",
    GRAT:  "https://www.kmlproject.com/Launcher/LKamougratulations.php",
    TACLIC:  "https://www.kmlproject.com/Launcher/LKamoutaclic.php",
}

class KMLAPIAction(AAction):
    """Runs KMLAPI action. Inherits from AAction.
    The message is retrieved with a call to the KML Project API.
    """
    _type = lang.KMLAPI_ACTION
    __url = None
    __random = False
    __answer = None

    def init(self, _argument_table):
        argument_table = _argument_table[_argument_table.index(self._type)+1:]
        if not len(argument_table) or argument_table[0] == lang.RANDOM_VAR:
            self.__random = True
        elif argument_table[0].lower() in KAMOU.keys():
            self.__url = KAMOU[argument_table[0].lower()]
        else:
            raise herror.ActionHasselError(herror.INVALIDLINE, _argument_table)

    def run(self, message, sender):
        """Makes a call to the KML Project API according to the parameters
        set in config. Choose a random URL if random is set.
        """
        if self.__random:
            message = message.split()
            if len(message) >= 2 and message[1].lower() in KAMOU.keys():
                self.__answer = urllib2.urlopen(KAMOU[message[1].lower()]).read()
            else:
                self.__answer = urllib2.urlopen(KAMOU[choice(KAMOU.keys())]).read()
        else:
            self.__answer = urllib2.urlopen(self.__url).read()
        return self.__answer

#-----------------------------------------------------------------------------#
# BINGO ACTION                                                                #
# In config: DO BINGO bingofile                                               #
# Play bingo with words from bingo files                                      #
#-----------------------------------------------------------------------------#

class BingoAction(AAction):
    """Runs BINGO action. Inherits from AAction.
    Bingo commands takes argument to join, see grid, validate words, etc.
    Instanciate Bingo class from Hasselib.game.
    """
    _type = lang.BINGO_ACTION
    __bingo = None

    def init(self, _argument_table):
        self.__bingo = bingo.Bingo()
        argument_table = _argument_table[_argument_table.index(self._type)+1:]
        for entry in argument_table:
            base = path.join(htools.get_file_directory(), lang.DATABASE_DIR, entry)
            if htools.is_file_access(base):
                self.__bingo.add_base_files(base)

    def run(self, message, sender):
        """Calls Bingo run :)"""
        self.__answer = self.__bingo.run(message, sender)
        is_bingo = self.__bingo.is_bingo()
        if is_bingo:
            self.__answer = [self.__answer, is_bingo]
        return self.__answer

#-----------------------------------------------------------------------------#
# INSULTE ACTION                                                              #
# In config: DO INSULTE content                                               #
# Display message generated with Markov chain using content or files          #
#-----------------------------------------------------------------------------#

class InsulteAction(AAction):
    """Runs INSULTE action. Inherits from AAction.
    The message printed in defined in the configuration file. It can contain
    variables which will be replaced at run.
    """
    _type = lang.INSULTE_ACTION
    __insulte_manager = None
    __answer = None

    def init(self, _argument_table):
        self.__insulte_manager = insulte.InsulteManager()
        argument_table = _argument_table[_argument_table.index(self._type)+1:]
        for entry in argument_table:
            insulte_file = path.join(htools.get_file_directory(), lang.INSULTE_DIR, entry)
            if htools.is_file_access(insulte_file):
                self.__insulte_manager.add(insulte_file)

    def run(self, message, sender):
        """Check if the condition that this objects looks for is met. Returns
        a boolean: True if condition is met, else False.
        Default is True.
        """
        receiver = self.__insulte_manager.get_receiver(message, sender)
        self.__answer = self.__insulte_manager.answer(receiver, sender)
        return self.__answer
