# -*- coding: utf-8 -*-
#
# Test for Hasselhov - Lex2xS
#
# pylint: disable=invalid-name

###############################################################################
# IMPORTS                                                                     #
###############################################################################

from __future__ import print_function
from sys import argv

try:
    from hasselib import herror
    from hasselib.language import markov
except ImportError as ie:
    print("Error: " + str(ie) + " in " + str(__file__))
    exit()

###############################################################################
# TEST RUN                                                                    #
###############################################################################

if __name__ == '__main__':
    try:
        hasselhov = markov.Markov()
        hasselhov.add_base_dir("base")
        if argv[1:]:
            answer = hasselhov.answer(" ".join(argv[1:]))
            print(answer)
    except NotImplementedError as nie:
        print("Error: Method is not implemented: " + str(nie))
    except KeyboardInterrupt as kie:
        print(str(kie))
