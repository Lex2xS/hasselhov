# -*- coding: utf-8 -*-

"""Hasselbot is Hasselhov's IRC bot. It contains the
commands and behaviors available in the Hasselhov project.

Behaviors can be configured by:
- Using command line arguments as set in help (see below for extract)
- Modifying the configuration file (syntax and info in config file and README)

HELP :
usage: hasselbot.py [-h] [-v] [-hh HOST] [-p PORT] [-c CHAN] [-n NICK]
                    [-d DESC] [-m MSTR]

Hasselhov is a very clever IRC bot with pluggable modules.

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         Verbose mode (mostly for debug)
  -hh HOST, --host HOST
                        IRC server to connect to (default is irc.freenode.net)
  -p PORT, --port PORT  IRC port to connect to (default is 6667)
  -c CHAN, --chan CHAN  IRC chan to connect to on server (default is
                        #hasselhov)
  -n NICK, --nick NICK  Bot nickname (default is HasselhovBot)
  -d DESC, --desc DESC  Bot description (displayed when entering a chan)
  -m MSTR, --mstr MSTR  Nickname of the person allowed to command the bot
"""

###############################################################################
# IMPORTS                                                                     #
###############################################################################

from __future__ import print_function
from argparse import ArgumentParser
from sys import argv

try:
    import hasselang as lang
    from hasselib import herror, htools
    from hasselib.extern import irclib, ircbot
    from modules import module
except ImportError as ie:
    print("Error: " + str(ie) + " in " + str(__file__))
    exit()

###############################################################################
# CONSTANTS                                                                   #
###############################################################################

CONFIG_FILE = "modules/hasselconfig.txt"
COMMENT_TAG = "#"
TRIGGER_TAG = "ON"
ACTION_TAG = "DO"

# DEBUG
VERBOSE_BOOL = False
def VERBOSE(action, message, sender=""):
    if VERBOSE_BOOL:
        print("[HASSELHOV] {0}: {1} ({2})".format(action, message, sender))

# DEFAULT INDEXES FOR OPTIONS
NAME = 0
S_OPT = 1
L_OPT = 2
DEFAULT = 3

# OPTIONS
O_HOST = ("host", "-hh", "--host", "irc.freenode.net")
O_PORT = ("port", "-p", "--port", 6667)
O_CHAN = ("chan", "-c", "--chan", "#hasselhov")
O_NICK = ("nick", "-n", "--nick", lang.BOT_NICKNAME)
O_DESC = ("desc", "-d", "--desc", lang.BOT_DESCRIPTION)
O_MSTR = ("mstr", "-m", "--mstr", lang.BOT_MASTER)
O_CONF = ("conf", "-cf", "--conf", CONFIG_FILE)
O_VERBOSE = ("verbose", "-v", "--verbose", VERBOSE_BOOL)


###############################################################################
# HASSELBOT CLASS                                                             #
###############################################################################

class Hasselbot(object):
    """IRC bot class. Is intentiated by a HIrcBotClass
    All events in IRC are caught in HIrcBot class and redirected
    in Hasselbot classes.
    """
    __conf = None # Configuration file path
    __modules = None # Module class

    def __init__(self, irc_instance):
        self.__irc = irc_instance
        self.__set_arguments()
        self.__set_modules()

    # Initialization

    def __set_arguments(self):
        """Initialize command line arguments."""
        def init_arguments():
            """Initialize ArgumentParser object in nested."""
            args = ArgumentParser(description=lang.HELP)
            args.add_argument(O_VERBOSE[S_OPT], O_VERBOSE[L_OPT],
                              action='store_true', help=lang.H_VERB)
            args.add_argument(O_HOST[S_OPT], O_HOST[L_OPT], help=lang.H_HOST)
            args.add_argument(O_PORT[S_OPT], O_PORT[L_OPT], help=lang.H_PORT)
            args.add_argument(O_CHAN[S_OPT], O_CHAN[L_OPT], help=lang.H_CHAN)
            args.add_argument(O_NICK[S_OPT], O_NICK[L_OPT], help=lang.H_NICK)
            args.add_argument(O_DESC[S_OPT], O_DESC[L_OPT], help=lang.H_DESC)
            args.add_argument(O_MSTR[S_OPT], O_MSTR[L_OPT], help=lang.H_MSTR)
            args.add_argument(O_CONF[S_OPT], O_CONF[L_OPT], help=lang.H_CONF)
            return args.parse_args()
        args = init_arguments()
        # Special debug arguments
        global VERBOSE_BOOL
        VERBOSE_BOOL = args.verbose if args.verbose else O_VERBOSE[DEFAULT]
        # We need these everywhere
        lang.BOT_NICKNAME = args.nick if args.nick else O_NICK[DEFAULT]
        lang.BOT_MASTER = args.mstr if args.mstr else O_MSTR[DEFAULT]
        # COnfiguration data (it must be under source tree)
        self.__conf = htools.realpath(args.conf if args.conf else O_CONF[DEFAULT], force=True)
        # Info used in IRC instance
        self.__irc.args[O_HOST[NAME]] = args.host if args.host else O_HOST[DEFAULT]
        self.__irc.args[O_PORT[NAME]] = args.port if args.port else O_PORT[DEFAULT]
        self.__irc.args[O_CHAN[NAME]] = args.chan if args.chan else O_CHAN[DEFAULT]
        self.__irc.args[O_NICK[NAME]] = args.nick if args.nick else O_NICK[DEFAULT]
        self.__irc.args[O_DESC[NAME]] = args.desc if args.desc else O_DESC[DEFAULT]
        # Requires # before chan name
        if not self.__irc.args[O_CHAN[NAME]].startswith("#"):
            self.__irc.args[O_CHAN[NAME]] = "#"+self.__irc.args[O_CHAN[NAME]]
        VERBOSE("INIT", "Host: {0} | Port: {1} | Chan: {2}".format(self.__irc.args[O_HOST[NAME]],
                                                                   self.__irc.args[O_PORT[NAME]],
                                                                   self.__irc.args[O_CHAN[NAME]]), "")
        VERBOSE("INIT", "Nick: {0} | Desc: {1}".format(self.__irc.args[O_NICK[NAME]],
                                                       self.__irc.args[O_DESC[NAME]]), "")

    # Module initialization

    def __set_modules(self):
        """Initialize modules from configuration file."""
        self.__modules = {}
        if not htools.is_file_access(self.__conf):
            herror.error(lang.E_CONFIG)
        try:
            with open(self.__conf, 'r') as fd:
                config = [x.strip() for x in fd if len(x) > 2 and not x.startswith(COMMENT_TAG)]
        except IOError as ioe:
            herror.error(ioe)
        # Start parsing and creating modules
        in_module = False
        counter = 0
        for line in config:
            line = line.split(COMMENT_TAG)[0].strip()
            if line.startswith(TRIGGER_TAG): # BEGIN
                in_module = True
                counter += 1
                trigger = line.split()[1:]
            elif in_module and line.startswith(ACTION_TAG): # STOP
                in_module = False
                action = line.split()[1:]
                # MODULE CREATION (raises ModuleHasselError)
                try:
                    if not len(trigger):
                        raise herror.ModuleHasselError(lang.W_SYNTAX, trigger)
                    if not len(action):
                        raise herror.ModuleHasselError(lang.W_SYNTAX, action)
                    self.__modules[counter] = module.Module(trigger, action, counter)
                except herror.ModuleHasselError as mhe:
                    herror.warning(str(mhe))
        # print([str(x) for _, x in self.__modules.iteritems()])

    # RELOAD

    def __reload(self):
        """Reload all classes and modules."""
        # Data
        reload(module)
        module.do_reload()
        self.__set_modules()
        return lang.MSG_RELOAD

    # Events

    def on_join(self, sender):
        """When someone joins the channel"""
        for _, module in self.__modules.iteritems():
            if module.event == lang.JOIN_EVENT:
                if module.trigger.is_triggered("", sender):
                    return module.action.run("", sender)
        return None

    def on_message(self, message, sender):
        """When someone publishes a message on the channel."""
        if message == lang.CMD_RELOAD and sender == lang.BOT_MASTER:
            return self.__reload()
        for _, module in self.__modules.iteritems():
            if module.event == lang.MESSAGE_EVENT:
                if module.trigger.is_triggered(message, sender):
                    return module.action.run(message, sender)
        return None

    def on_privatemessage(self, message, sender):
        """When receiving a private message."""
        pass

    def on_kick(self, message, sender):
        """When someone gets kicked."""
        pass

    def on_quit(self, message, sender):
        """When leaving."""
        pass

###############################################################################
# IRCBOT INTERFACE CLASS, should not be modified                              #
###############################################################################

class HasselIrcBot(ircbot.SingleServerIRCBot):
    """Proxy to IRC lib functions."""
    # Public
    silent = False # Boolean
    args = {} # Dictionary of options
    # Private
    __hasselhov = None # Instance of Hasselhov

    # Initialisation

    def __init__(self):
        self.__hasselhov = Hasselbot(self)
        self.__connect()


    # Connection / Disconnection

    def __connect(self):
        """Connect to IRC."""
        try:
            ircbot.SingleServerIRCBot.__init__(self,
                                               [(self.args[O_HOST[NAME]],
                                                 self.args[O_PORT[NAME]])],
                                               self.args[O_NICK[NAME]],
                                               self.args[O_DESC[NAME]])
        except IndexError as ie:
            herror.warning(lang.E_OPTIONS, str(ie))
        VERBOSE("CONNECT", "{0}@{1}".format(self.args[O_NICK[NAME]], self.args[O_HOST[NAME]]))

    def __disconnect(self):
        """Disconnect from IRC."""
        raise NotImplementedError("Hasselbot: disconnect")

    def __set_tg(self, serv):
        """Set and unset TG mode"""
        self.silent = not self.silent
        serv.nick("{0}_{1}".format(lang.BOT_NICKNAME, "tg") \
                  if self.silent else lang.BOT_NICKNAME)

    def talk(self, serv, target, answer, sender, action=""):
        """Send answer on channel if allowed to."""
        if answer and not self.silent:
            if type(answer) is str:
                answer = [answer]
            for line in answer:
                serv.privmsg(target, line)
                VERBOSE(action, line, sender)

    # Events

    def on_welcome(self, serv, ev):
        """When connect to the IRC server, we join the channel"""
        serv.join(self.args["chan"])
        VERBOSE("CONNECT", self.args["chan"])

    def on_join(self, serv, ev):
        """When someone joins the channel"""
        sender = irclib.nm_to_n(ev.source())
        answer = self.__hasselhov.on_join(sender)
        self.talk(serv, ev.target(), answer, "", "JOIN")

    def on_pubmsg(self, serv, ev):
        """When someone publishes a message on the channel.
        TODO: Add queue system.
        """
        # SILENT COMMAND
        if ev.arguments()[0] in [lang.CMD_TG, lang.CMD_TGSPE]:
            return self.__set_tg(serv)
        sender = irclib.nm_to_n(ev.source())
        answer = self.__hasselhov.on_message(ev.arguments()[0], sender)
        self.talk(serv, ev.target(), answer, sender, "MESSAGE")

    def on_privmsg(self, serv, ev):
        """When receiving a private message."""
        sender = irclib.nm_to_n(ev.source())
        answer = self.__hasselhov.on_privatemessage(ev.arguments()[0], sender)
        self.talk(serv, ev.target(), answer, sender, "PRIVATE")

    def on_kick(self, serv, ev):
        """When someone gets kicked."""
        sender = irclib.nm_to_n(ev.source())
        answer = self.__hasselhov.on_kick(ev.arguments()[0], sender)
        self.talk(serv, ev.target(), answer, sender, "KICK")

    def on_quit(self, serv, ev):
        """When leaving."""
        sender = irclib.nm_to_n(ev.source())
        answer = self.__hasselhov.on_quit(ev.arguments()[0], sender)
        self.talk(serv, ev.target(), answer, sender, "QUIT")

if __name__ == "__main__":
    try:
        HasselIrcBot().start()
    except NotImplementedError as nie:
        print(str(nie))
    except KeyboardInterrupt as ke:
        print("Exit.")
