# -*- coding: utf-8 -*-

"""Hasselang contains all the text messages in the application."""

###############################################################################
# CONSTANTS                                                                   #
###############################################################################

# VERY BASIC AND USEFUL GLOBAL INFO
BOT_NICKNAME = "Hasselhov" # Will be changed with arguments
BOT_DESCRIPTION = "Oh, hi!"
BOT_MASTER = "Lex2xS"

DATABASE_DIR = "base"
INSULTE_DIR = "base/insultes"

# Default builtin commands

CMD_RELOAD = "!reload"
MSG_RELOAD = "Reload OK"

CMD_TG = "!tg"
CMD_TGSPE = "!hasseltg"

# HELP

HELP = "Hasselhov is a very clever IRC bot with pluggable modules."
H_HOST = "IRC server to connect to (default is irc.freenode.net)"
H_PORT = "IRC port to connect to (default is 6667)"
H_CHAN = "IRC chan to connect to on server (default is #hasselhov)"
H_NICK = "Bot nickname (default is HasselhovBot)"
H_DESC = "Bot description (displayed when entering a chan)"
H_MSTR = "Nickname of the person allowed to command the bot"
H_CONF = "Configuration file for modules loading"
H_VERB = "Verbose mode (mostly for debug)"

# CONFIGURATION FILE DATA

JOIN_EVENT = "JOIN"
MESSAGE_EVENT = "MESSAGE"

CONTAINS_TRIGGER = "CONTAINS"
COMMAND_TRIGGER = "COMMAND"
SENDER_TRIGGER = "SENDER"

PRINT_ACTION = "PRINT"
RANDOM_ACTION = "RANDOM"
MARKOV_ACTION = "MARKOV"
KMLAPI_ACTION = "KMLAPI"
BINGO_ACTION = "BINGO"
INSULTE_ACTION = "INSULTE"

FILE_KEYWORD = "FILE"

# VARIABLES

VAR_TAG = "%"

ME_VAR = "%ME%"
ALL_VAR = "%ALL%"
SENDER_VAR = "%SENDER%"
MASTER_VAR = "%MASTER%"
RANDOM_VAR = "%RANDOM%"

# WARNINGS

W_SYNTAX = "Syntax error"
W_UNKNOWNTYPE_T = "Unknown trigger type"
W_UNKNOWNTYPE_A = "Unknown action type"

# ERRORS

E_OPTIONS = "Missing options"
E_CONFIG = "Missing configuration file"

# INSUKTES

INSULTE_PRE = u"espèce de"
INSULTE_TAG = "insulte"
PRESULTE = "presulte"
SULTE = "sulte"
POSTSULTE = "postsulte"

# INTERNAL REGEX

LINESPLIT = "[\.!\n]+"

# French Grammar

GRAMMAR = {
    " de e": " d'e",
    " de é": " d'é",
    " de a": " d'a",
    " de i": " d'i"
}
